/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Defines the session of RootScene.
 * @syscap SystemCapability.ArkUI.ArkUI.Full
 * @systemapi
 * @since 10
 */
interface RootSceneSession {
}

/**
 * Defines the interface of RootScene.
 * @syscap SystemCapability.ArkUI.ArkUI.Full
 * @systemapi
 * @since 10
 */
interface RootSceneInterface {
  /**
   * Called when the RootScene is used.
   * @param { RootSceneSession } session - indicates the session of RootScene.
   * @syscap SystemCapability.ArkUI.ArkUI.Full
   * @systemapi
   * @returns { RootSceneAttribute }
   * @since 10
   */
  (session: RootSceneSession): RootSceneAttribute;
}

/**
 * Defines the attribute functions of RootScene.
 * @syscap SystemCapability.ArkUI.ArkUI.Full
 * @systemapi
 * @since 10
 */
declare class RootSceneAttribute extends CommonMethod<RootSceneAttribute> {
}

/**
 * Defines the RootScene component.
 * @syscap SystemCapability.ArkUI.ArkUI.Full
 * @systemapi
 * @since 10
 */
declare const RootScene: RootSceneInterface;

/**
 * Defines the RootScene instance.
 * @syscap SystemCapability.ArkUI.ArkUI.Full
 * @systemapi
 * @since 10
 */
declare const RootSceneInstance: RootSceneAttribute;

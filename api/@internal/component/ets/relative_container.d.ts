/*
 * Copyright (c) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Provides ports for relative containers.
 * @form
 * @since 9
 */
/**
 * Provides ports for relative containers.
 * @form
 * @crossplatform
 * @since 10
 */
interface RelativeContainerInterface {
  (): RelativeContainerAttribute;
}

/**
 * @form
 * @since 9
 */
/**
 * @form
 * @crossplatform
 * @since 10
 */
declare class RelativeContainerAttribute extends CommonMethod<RelativeContainerAttribute> {
}

/**
 * @form
 * @since 9
 */
/**
 * @form
 * @crossplatform
 * @since 10
 */
declare const RelativeContainer: RelativeContainerInterface;

/**
 * @form
 * @since 9
 */
/**
 * @form
 * @crossplatform
 * @since 10
 */
declare const RelativeContainerInstance: RelativeContainerAttribute;
